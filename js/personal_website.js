$(document).ready(function() { 

document.getElementById("about_window").style.background = "red";
//document.getElementById("projects_window").style.background = "orange";
document.getElementById("contact_window").style.background = "brown";

$("#home")[0].onclick = function() { on_menu_select($("#home_window"))};
$("#about")[0].onclick = function() { on_menu_select($("#about_window"))};
$("#projects")[0].onclick = function() { on_menu_select($("#projects_window"))};
$("#contact")[0].onclick = function() { on_menu_select($("#contact_window"))};

$("#projects").mouseenter ( on_mouse_enter);
$("#projects").mouseleave ( function() { on_mouse_leave( $("#border_menu_bottom") )});
$("#border_menu_bottom").mouseleave ( function() { on_mouse_leave( $("#projects") )});

});

function on_menu_select (content_div)
{
	if(content_div.css("display") == "none")
	{
		$(".content").animate(
			{opacity: '0'}, 
			"fast", 
			function() {
				$(".content").hide();
				content_div.show();
				content_div.animate({opacity: '1'}, "slow");
			}
		);
	}
}

function on_mouse_enter ()
{
	$("#border_menu_bottom").stop();
	$("#border_menu_bottom").slideDown('1000');
}

function on_mouse_leave ( buddy_menu_item )
{
	if (!(buddy_menu_item.is(":hover")))
	{
		$("#border_menu_bottom").stop();
		$("#border_menu_bottom").slideUp('1000');
	}
}